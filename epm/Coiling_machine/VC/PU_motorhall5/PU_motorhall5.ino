//Wonder Winder
//Version: Counter only, motor powered externally (Mar 1, 2017)
//dace.de

// LCD implemented (30/05/2017)

//Todo:
//-restart feature to wind next coil using button
//-display on Ardu
//-use poti to set number of windings (although not the winding speed IMO)

//speed 1: 400 rpm
//speed 2: 700 rpm
//speed 3: 900 rpm

#include "LCD.h"

////////////// SET WINDINGS HERE ////////////////////////////////////////////////////

int windings = 210;  //to give you an idea: Gibson PAF: 42 gauge plain enamel, 5000 windings CCW, 4.0 average resistance DC, Alnico 2 or 3 magnet  

/////////////////////////////////////////////////////////////////////////////////////

const int motorPin = 9;
// LED pin overlaps with SCLK for SPI communication with LCD 
//const int ledPin = 13;

//relay control pins
const int iRelay1=4;
//const int iRelay2=7;
//const int iRelay3=8;
//const int iRelay4=12;

// digital pin 2 has a pushbutton attached to it. Give it a name:
int counterPin = 2;
int oldButtonState = digitalRead(counterPin);
int count = 0;
long lastmillis = 0.0;
long nowmillis = 0.0;

int iWindingsUndercut = 50; //(int)(((float)windings) * 0.1);  //number of windings to stop the motor; TODO: find number of windings, not %
boolean bDone = false;
boolean bMotor = false;
long lTicksStart=millis();

void setup()
{
  Serial.begin(9600);  //set up the serial port
  
  // LCD setup
  lcdBegin(); // This will setup our pins, and initialize the LCD
  updateDisplay(); // with displayMap untouched, SFE logo
  setContrast(40); // Good values range from 40-60
  delay(2000);
 
  wrLCD("~");
  updateDisplay();

  // Coiling machine setup
  
  //pinMode(ledPin, OUTPUT);  //set up the LED pin to be an output
  pinMode(motorPin, OUTPUT);  //set up the motor pin to be an output
  pinMode(counterPin, INPUT);  //make the pushbutton's pin an input (i.e. sensor pin)

  pinMode(iRelay1, OUTPUT);
  digitalWrite(iRelay1, LOW);  //turn relay off at startup
  Serial.println();
  Serial.println();
  Serial.println("Wonder Winder");
  Serial.println();

  // print out the state of the button:
  Serial.print("Starting at ");
  Serial.print(lTicksStart);
  Serial.print(" ms to wind ");
  Serial.print(windings);
  Serial.println(" windings");
  
  char dString[75];
  sprintf(dString, "Starting at %d ms ",lTicksStart); 
  wrLCD(dString);
  sprintf(dString,"to wind %d windings.\n",windings);
  wrLCD(dString);
}

void loop()
{
  char dString[75];
  
  // Coiling machine loop 
  
  if (!bDone)
  {
    if (count*0.5 == 2)  //start timer after 2 windings when things are running
    {
      lTicksStart=millis();
    }
    if (count >= windings * 2) //stop motor after n windings
    {
      bDone = true;
      Serial.println("Job done");
      setStr("Job done",0,0,BLACK);
      
      Serial.print("Wound ");
      Serial.print(((float)count)*0.5);
      Serial.print(" windings in ");
      float fTicksElapsed=millis()-lTicksStart;
      Serial.print(fTicksElapsed*0.001);
      Serial.println(" s. ");
      Serial.print((((float)count*0.5)-2)/((fTicksElapsed*0.001)/60));
      Serial.println(" rpm. Have a nice day!");

      sprintf(dString,"~Wound %d windings ",(int)(((float)count)*0.5));
      wrLCD(dString);
      sprintf(dString,"in %d s.\n",(int)(fTicksElapsed*0.001));
      wrLCD(dString);
      sprintf(dString,"%d rpm.",(int)((((float)count*0.5)-2)/((fTicksElapsed*0.001)/60)));
      wrLCD(dString);
    }
  }

  // read the input pin:
  int buttonState = digitalRead(counterPin);
  //if (buttonState) { digitalWrite(ledPin, LOW); } else { digitalWrite(ledPin, HIGH); }
  nowmillis = millis();
  if (buttonState != oldButtonState)
  {
    oldButtonState = buttonState;
    count++;

    if (bDone)
    {
      Serial.print("Overwound: There are now ");
      Serial.print(((float)count)*0.5);
      Serial.println(" windings");

      sprintf(dString,"~There are now %d windings.\n",(int)(((float)count)*0.5));
      wrLCD(dString);
    }

    else if (count!=0 && count%10==0)  //output every 25 turns
    {
      Serial.print(((float)count)*0.5);
      Serial.println(" windings");

      sprintf(dString,"~%d windings.",(int)(((float)count)*0.5));
      wrLCD(dString);
    }
  }

  // print out the state of the button:
  /*
  Serial.print(nowmillis);
  Serial.print(" - ");
  Serial.print(buttonState);
  Serial.print(" - 2 * windings = ");
  Serial.println(count);
  */

  delay(1);  //delay in between reads for stability (needed?)
}

void wrLCD (char * dString,...)
{
  static int cursorX = 0;
  static int cursorY = 0;
  
  char i=0;
  
  do 
  {
    switch (dString[i])
    {
      case '\n': // New line
        cursorY += 8;
        break;
      case '\r': // Return feed
        cursorX = 0;
        break;
      case '~': // Use ~ to clear the screen.
        clearDisplay(WHITE);
        updateDisplay();
        cursorX = 0; // reset the cursor
        cursorY = 0;
        break;
      default:
        setChar(dString[i], cursorX, cursorY, BLACK);
        updateDisplay();
        cursorX += 6; // Increment cursor
        break;
      }
      
      // Manage cursor
      if (cursorX >= (LCD_WIDTH - 4)) 
      { // If the next char will be off screen...
        cursorX = 0; // ... reset x to 0...
        cursorY += 8; // ...and increment to next line.
        if (cursorY >= (LCD_HEIGHT - 7))
        { // If the next line takes us off screen...
          cursorY = 0; // ...go back to the top.
        }
      }

      i++;
  } while (dString[i]!='\0');
}

// Because I keep forgetting to put bw variable in when setting...
void setPixel(int x, int y)
{
  setPixel(x, y, BLACK); // Call setPixel with bw set to Black
}

void clearPixel(int x, int y)
{
  setPixel(x, y, WHITE); // call setPixel with bw set to white
}

// This function sets a pixel on displayMap to your preferred
// color. 1=Black, 0= white.
void setPixel(int x, int y, boolean bw)
{
  // First, double check that the coordinate is in range.
  if ((x >= 0) && (x < LCD_WIDTH) && (y >= 0) && (y < LCD_HEIGHT))
  {
    byte shift = y % 8;
  
    if (bw) // If black, set the bit.
      displayMap[x + (y/8)*LCD_WIDTH] |= 1<<shift;
    else   // If white clear the bit.
      displayMap[x + (y/8)*LCD_WIDTH] &= ~(1<<shift);
  }
}

// setLine draws a line from x0,y0 to x1,y1 with the set color.
// This function was grabbed from the SparkFun ColorLCDShield 
// library.
void setLine(int x0, int y0, int x1, int y1, boolean bw)
{
  int dy = y1 - y0; // Difference between y0 and y1
  int dx = x1 - x0; // Difference between x0 and x1
  int stepx, stepy;

  if (dy < 0)
  {
    dy = -dy;
    stepy = -1;
  }
  else
    stepy = 1;

  if (dx < 0)
  {
    dx = -dx;
    stepx = -1;
  }
  else
    stepx = 1;

  dy <<= 1; // dy is now 2*dy
  dx <<= 1; // dx is now 2*dx
  setPixel(x0, y0, bw); // Draw the first pixel.

  if (dx > dy) 
  {
    int fraction = dy - (dx >> 1);
    while (x0 != x1)
    {
      if (fraction >= 0)
      {
        y0 += stepy;
        fraction -= dx;
      }
      x0 += stepx;
      fraction += dy;
      setPixel(x0, y0, bw);
    }
  }
  else
  {
    int fraction = dx - (dy >> 1);
    while (y0 != y1)
    {
      if (fraction >= 0)
      {
        x0 += stepx;
        fraction -= dy;
      }
      y0 += stepy;
      fraction += dx;
      setPixel(x0, y0, bw);
    }
  }
}

// setRect will draw a rectangle from x0,y0 top-left corner to
// a x1,y1 bottom-right corner. Can be filled with the fill
// parameter, and colored with bw.
// This function was grabbed from the SparkFun ColorLCDShield
// library.
void setRect(int x0, int y0, int x1, int y1, boolean fill, boolean bw)
{
  // check if the rectangle is to be filled
  if (fill == 1)
  {
    int xDiff;

    if(x0 > x1)
      xDiff = x0 - x1; //Find the difference between the x vars
    else
      xDiff = x1 - x0;

    while(xDiff > 0)
    {
      setLine(x0, y0, x0, y1, bw);

      if(x0 > x1)
        x0--;
      else
        x0++;

      xDiff--;
    }
  }
  else 
  {
    // best way to draw an unfilled rectangle is to draw four lines
    setLine(x0, y0, x1, y0, bw);
    setLine(x0, y1, x1, y1, bw);
    setLine(x0, y0, x0, y1, bw);
    setLine(x1, y0, x1, y1, bw);
  }
}

// setCircle draws a circle centered around x0,y0 with a defined
// radius. The circle can be black or white. And have a line
// thickness ranging from 1 to the radius of the circle.
// This function was grabbed from the SparkFun ColorLCDShield 
// library.
void setCircle (int x0, int y0, int radius, boolean bw, int lineThickness)
{
  for(int r = 0; r < lineThickness; r++)
  {
    int f = 1 - radius;
    int ddF_x = 0;
    int ddF_y = -2 * radius;
    int x = 0;
    int y = radius;

    setPixel(x0, y0 + radius, bw);
    setPixel(x0, y0 - radius, bw);
    setPixel(x0 + radius, y0, bw);
    setPixel(x0 - radius, y0, bw);

    while(x < y)
    {
      if(f >= 0)
      {
        y--;
        ddF_y += 2;
        f += ddF_y;
      }
      x++;
      ddF_x += 2;
      f += ddF_x + 1;

      setPixel(x0 + x, y0 + y, bw);
      setPixel(x0 - x, y0 + y, bw);
      setPixel(x0 + x, y0 - y, bw);
      setPixel(x0 - x, y0 - y, bw);
      setPixel(x0 + y, y0 + x, bw);
      setPixel(x0 - y, y0 + x, bw);
      setPixel(x0 + y, y0 - x, bw);
      setPixel(x0 - y, y0 - x, bw);
    }
    radius--;
  }
}

// This function will draw a char (defined in the ASCII table
// near the beginning of this sketch) at a defined x and y).
// The color can be either black (1) or white (0).
void setChar(char character, int x, int y, boolean bw)
{
  byte column; // temp byte to store character's column bitmap
  for (int i=0; i<5; i++) // 5 columns (x) per character
  {
    column = ASCII[character - 0x20][i];
    for (int j=0; j<8; j++) // 8 rows (y) per character
    {
      if (column & (0x01 << j)) // test bits to set pixels
        setPixel(x+i, y+j, bw);
      else
        setPixel(x+i, y+j, !bw);
    }
  }
}

// setStr draws a string of characters, calling setChar with
// progressive coordinates until it's done.
// This function was grabbed from the SparkFun ColorLCDShield
// library.
void setStr(char * dString, int x, int y, boolean bw)
{
  while (*dString != 0x00) // loop until null terminator
  {
    setChar(*dString++, x, y, bw);
    x+=5;
    for (int i=y; i<y+8; i++)
    {
      setPixel(x, i, !bw);
    }
    x++;
    if (x > (LCD_WIDTH - 5)) // Enables wrap around
    {
      x = 0;
      y += 8;
    }
  }
}

// This function will draw an array over the screen. (For now) the
// array must be the same size as the screen, covering the entirety
// of the display.
void setBitmap(char * bitArray)
{
  for (int i=0; i<(LCD_WIDTH * LCD_HEIGHT / 8); i++)
    displayMap[i] = bitArray[i];
}

// This function clears the entire display either white (0) or
// black (1).
// The screen won't actually clear until you call updateDisplay()!
void clearDisplay(boolean bw)
{
  for (int i=0; i<(LCD_WIDTH * LCD_HEIGHT / 8); i++)
  {
    if (bw)
      displayMap[i] = 0xFF;
    else
      displayMap[i] = 0;
  }
}

// Helpful function to directly command the LCD to go to a 
// specific x,y coordinate.
void gotoXY(int x, int y)
{
  LCDWrite(0, 0x80 | x);  // Column.
  LCDWrite(0, 0x40 | y);  // Row.  ?
}

// This will actually draw on the display, whatever is currently
// in the displayMap array.
void updateDisplay()
{
  gotoXY(0, 0);
  for (int i=0; i < (LCD_WIDTH * LCD_HEIGHT / 8); i++)
  {
    LCDWrite(LCD_DATA, displayMap[i]);
  }
}

// Set contrast can set the LCD Vop to a value between 0 and 127.
// 40-60 is usually a pretty good range.
void setContrast(byte contrast)
{  
  LCDWrite(LCD_COMMAND, 0x21); //Tell LCD that extended commands follow
  LCDWrite(LCD_COMMAND, 0x80 | contrast); //Set LCD Vop (Contrast): Try 0xB1(good @ 3.3V) or 0xBF if your display is too dark
  LCDWrite(LCD_COMMAND, 0x20); //Set display mode
}

/* There are two ways to do this. Either through direct commands
to the display, or by swapping each bit in the displayMap array.
We'll leave both methods here, comment one or the other out if 
you please. */
void invertDisplay()
{
  /* Direct LCD Command option
  LCDWrite(LCD_COMMAND, 0x20); //Tell LCD that extended commands follow
  LCDWrite(LCD_COMMAND, 0x08 | 0x05); //Set LCD Vop (Contrast): Try 0xB1(good @ 3.3V) or 0xBF if your display is too dark
  LCDWrite(LCD_COMMAND, 0x20); //Set display mode  */
  
  /* Indirect, swap bits in displayMap option: */
  for (int i=0; i < (LCD_WIDTH * LCD_HEIGHT / 8); i++)
  {
    displayMap[i] = ~displayMap[i] & 0xFF;
  }
  updateDisplay();
}

// There are two memory banks in the LCD, data/RAM and commands.
// This function sets the DC pin high or low depending, and then 
// sends the data byte
void LCDWrite(byte data_or_command, byte data) 
{
  //Tell the LCD that we are writing either to data or a command
  digitalWrite(dcPin, data_or_command); 

  //Send the data
  digitalWrite(scePin, LOW);
  SPI.transfer(data); //shiftOut(sdinPin, sclkPin, MSBFIRST, data);
  digitalWrite(scePin, HIGH);
}

//This sends the magical commands to the PCD8544
void lcdBegin(void) 
{
  //Configure control pins
  pinMode(scePin, OUTPUT);
  pinMode(rstPin, OUTPUT);
  pinMode(dcPin, OUTPUT);
  pinMode(sdinPin, OUTPUT);
  pinMode(sclkPin, OUTPUT);
  pinMode(blPin, OUTPUT);
  analogWrite(blPin, 255);

  SPI.begin();
  SPI.setDataMode(SPI_MODE0);
  SPI.setBitOrder(MSBFIRST);
  
  //Reset the LCD to a known state
  digitalWrite(rstPin, LOW);
  digitalWrite(rstPin, HIGH);

  LCDWrite(LCD_COMMAND, 0x21); //Tell LCD extended commands follow
  LCDWrite(LCD_COMMAND, 0xB0); //Set LCD Vop (Contrast)
  LCDWrite(LCD_COMMAND, 0x04); //Set Temp coefficent
  LCDWrite(LCD_COMMAND, 0x14); //LCD bias mode 1:48 (try 0x13)
  //We must send 0x20 before modifying the display control mode
  LCDWrite(LCD_COMMAND, 0x20); 
  LCDWrite(LCD_COMMAND, 0x0C); //Set display control, normal mode.
}



