#define LED_PIN 13
#define EPM_PIN 10

#define MICROSECONDS 0.001

// Diameter of the wire used for the coil
#define AWG_WIRE_SIZE 30

#if AWG_WIRE_SIZE==24
  // Time constant of the current pulse for magnet of diameter x length
  #define CURRENT_PULSE_TC_4X6 (float)902*MICROSECONDS
  #define CURRENT_PULSE_TC_5X8 (float)1614*MICROSECONDS
  #define CURRENT_PULSE_TC_6X10 (float)2526*MICROSECONDS
  #define CURRENT_PULSE_TC_10X20 (float)100000*MICROSECONDS
#elif AWG_WIRE_SIZE==25
  // Time constant of the current pulse for magnet of diameter x length
  #define CURRENT_PULSE_TC_4X6 (float)877*MICROSECONDS
  #define CURRENT_PULSE_TC_5X8 (float)1551*MICROSECONDS
  #define CURRENT_PULSE_TC_6X10 (float)2395*MICROSECONDS
  #define CURRENT_PULSE_TC_10X20 (float)7478*MICROSECONDS
#elif AWG_WIRE_SIZE==26
  // Time constant of the current pulse for magnet of diameter x length
  #define CURRENT_PULSE_TC_4X6 (float)847*MICROSECONDS
  #define CURRENT_PULSE_TC_5X8 (float)1475*MICROSECONDS
  #define CURRENT_PULSE_TC_6X10 (float)2239*MICROSECONDS
  // Enough magnetic field intensity cannot be reached for magnet of 10 x 20 mm.
#elif AWG_WIRE_SIZE==27
  // Time constant of the current pulse for magnet of diameter x length
  #define CURRENT_PULSE_TC_4X6 (float)813*MICROSECONDS
  #define CURRENT_PULSE_TC_5X8 (float)1389*MICROSECONDS
  #define CURRENT_PULSE_TC_6X10 (float)2068*MICROSECONDS
  // Enough magnetic field intensity cannot be reached for magnet of 10 x 20 mm.
#elif AWG_WIRE_SIZE==28
  // Time constant of the current pulse for magnet of diameter x length
  #define CURRENT_PULSE_TC_4X6 (float)770*MICROSECONDS
  #define CURRENT_PULSE_TC_5X8 (float)1284*MICROSECONDS
  #define CURRENT_PULSE_TC_6X10 (float)1867*MICROSECONDS
  // Enough magnetic field intensity cannot be reached for magnet of 10 x 20 mm.
#elif AWG_WIRE_SIZE==29
  // Time constant of the current pulse for magnet of diameter x length
  #define CURRENT_PULSE_TC_4X6 (float)724*MICROSECONDS
  #define CURRENT_PULSE_TC_5X8 (float)1178*MICROSECONDS
  #define CURRENT_PULSE_TC_6X10 (float)1671*MICROSECONDS
  // Enough magnetic field intensity cannot be reached for magnet of 10 x 20 mm.
#elif AWG_WIRE_SIZE==30
  // Time constant of the current pulse for magnet of diameter x length
  #define CURRENT_PULSE_TC_4X6 (float)665*MICROSECONDS
  #define CURRENT_PULSE_TC_5X8 (float)1047*MICROSECONDS
  #define CURRENT_PULSE_TC_6X10 (float)1444*MICROSECONDS
  #define CURRENT_PULSE_TC_10X20 (float)5000*MICROSECONDS
  // Enough magnetic field intensity cannot be reached for magnet of 10 x 20 mm.
#elif AWG_WIRE_SIZE==31
  // Time constant of the current pulse for magnet of diameter x length
  #define CURRENT_PULSE_TC_4X6 (float)604*MICROSECONDS
  #define CURRENT_PULSE_TC_5X8 (float)920*MICROSECONDS
  // Enough magnetic field intensity cannot be reached for magnets of 6 x 10 mm and 10 x 20 mm.
#elif AWG_WIRE_SIZE==32
  // Time constant of the current pulse for magnet of diameter x length
  #define CURRENT_PULSE_TC_4X6 (float)537*MICROSECONDS
  #define CURRENT_PULSE_TC_5X8 (float)790*MICROSECONDS
  // Enough magnetic field intensity cannot be reached for magnets of 6 x 10 mm and 10 x 20 mm.
#elif AWG_WIRE_SIZE==33
  // Time constant of the current pulse for magnet of diameter x length
  #define CURRENT_PULSE_TC_4X6 (float)469*MICROSECONDS
  // Enough magnetic field intensity cannot be reached for magnets of 5 x 8 mm, 6 x 10 mm and 10 x 20 mm.
#elif AWG_WIRE_SIZE==34
  // Time constant of the current pulse for magnet of diameter x length
  #define CURRENT_PULSE_TC_4X6 (float)398*MICROSECONDS
  // Enough magnetic field intensity cannot be reached for magnets of 5 x 8 mm, 6 x 10 mm and 10 x 20 mm.
#elif AWG_WIRE_SIZE==35
  // Time constant of the current pulse for magnet of diameter x length
  #define CURRENT_PULSE_TC_4X6 (float)332*MICROSECONDS
  // Enough magnetic field intensity cannot be reached for magnets of 5 x 8 mm, 6 x 10 mm and 10 x 20 mm.
#endif

// Diameter of the magnet
#define EPM_DIAMETER 4

// Time constant of the current pulse for magnet
#if EPM_DIAMETER==4
  #define CURRENT_PULSE_TC CURRENT_PULSE_TC_4X6
#elif EPM_DIAMETER==5
  #define CURRENT_PULSE_TC CURRENT_PULSE_TC_5X8
 #elif EPM_DIAMETER==10
  #define CURRENT_PULSE_TC CURRENT_PULSE_TC_10X20
#endif

// A first-order system should achieve its permanent steady state after a time of 5* its time constant.
#define CURRENT_PULSE_TC_FACTOR 20

// Current pulse on time ('1' time)
#define CURRENT_PULSE_ON_TIME (unsigned long)CURRENT_PULSE_TC*CURRENT_PULSE_TC_FACTOR

// Total period of current pulse ('1' time + '0' time)
#define CURRENT_PULSE_PERIOD 1000

void setup() {
  // Initialization
  pinMode(EPM_PIN,OUTPUT); // electropermanent magnet = output
  pinMode(LED_PIN,OUTPUT); // LED = output
}


void loop () {
  unsigned long currentTime;

  currentTime=millis();

  // Switching LED state for display of the current pulse
  if (currentTime%CURRENT_PULSE_PERIOD<CURRENT_PULSE_ON_TIME) digitalWrite(LED_PIN,HIGH);
  else digitalWrite(LED_PIN,LOW);

  // Current pulse for the electropermanent (switching the state of the transistor on RAMPS board)
  if (currentTime%CURRENT_PULSE_PERIOD<CURRENT_PULSE_ON_TIME) digitalWrite(EPM_PIN,HIGH);
  else digitalWrite(EPM_PIN,LOW);
}
