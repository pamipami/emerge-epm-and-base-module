#include <SoftwareSerial.h>

#define BLUETOOTH_TX_PIN 2
#define BLUETOOTH_RX_PIN 4

#define UART_SPEED 9600
#define BLUETOOTH_UART_SPEED 9600

#define AX12_DEFAULT_ID 1
#define AX12_DEFAULT_UART_SPEED 1000000

#define BLUETOOTH_MAX_LENGTH 20

bool serialAvailable;
String strSerial; 

SoftwareSerial bluetoothSerial(BLUETOOTH_RX_PIN,BLUETOOTH_TX_PIN);

void setup() {  
  Serial.begin(UART_SPEED);
  bluetoothSerial.begin(BLUETOOTH_UART_SPEED);
}

void loop() {    
  if (Serial.available()) {
    strSerial=Serial.readStringUntil('\n'); 
    serialAvailable=1;
  }
  
  automateServomotor();
}

void automateServomotor() {
  typedef enum {idle,id,baudrate,duty,dutyID,dutyBaudrate,dutyMovements,dutyMove,dutyMoveSpeed,dutyEndless,dutyTurn,dutyMoveRW,dutyMoveSpeedRW,done} typeState;
  static typeState stateServomotor=idle; 

  static unsigned long int servomotorID; 
  static unsigned long int servomotorBaudrate; 
  
  char strBluetooth[BLUETOOTH_MAX_LENGTH],strBluetooth_i[BLUETOOTH_MAX_LENGTH];
  
  switch (stateServomotor) {
    case idle :
      Serial.println("Welcome to a proof of concept software allowing the control of AX-18A servomotors via Bluetooth.");
      Serial.print("Type baudrate of servomotor of interest : ");
      stateServomotor=baudrate;
    break;

    case baudrate :
      if (serialAvailable) {
        servomotorBaudrate=strSerial.toInt();
        
        Serial.print(strSerial);
        Serial.println(".");
        Serial.print("Type ID of servomotor of interest : ");
        
        serialAvailable=0;
        stateServomotor=id;
      }
      else stateServomotor=baudrate;
    break;

    case id :
      if (serialAvailable) {
        servomotorID=(unsigned char)strSerial.toInt();
        
        Serial.print(strSerial); 
        Serial.println(".");
        Serial.println("Type *1* to reset servomotor.");
        Serial.println("Type *2* to set ID of servomotor.");
        Serial.println("Type *3* to set baudrate of servomotor.");
        Serial.println("Type *4* to try movements of servomotor.");
        
        serialAvailable=0;
        stateServomotor=duty;
      }
      else stateServomotor=id;
    break;

    case duty :
      if (serialAvailable) {
        strSerial.toCharArray(strBluetooth,BLUETOOTH_MAX_LENGTH);
        
        Serial.print("You typed *");
        Serial.print(strSerial);
        Serial.println("*.");
        
        serialAvailable=0;

        switch (strSerial[0]) {
          case '1' :
            sprintf(strBluetooth,"R,%lu,%lu",servomotorBaudrate,servomotorID);
            bluetoothSerial.println(strBluetooth);

            servomotorID=AX12_DEFAULT_ID;
            servomotorBaudrate=AX12_DEFAULT_UART_SPEED;
            Serial.println("Servomotor of interest has been controlled.");
            Serial.println("Do you want to change servomotor of interest ? Type *n* for no or *y* for yes.");
            stateServomotor=done;
          break;
          
          case '2' :
            Serial.print("Type new ID of servomotor of interest : ");
            stateServomotor=dutyID;
          break;

          case '3' : 
            Serial.print("Type new baudrate of servomotor of interest : ");
            stateServomotor=dutyBaudrate;
          break;

          case '4' :
            Serial.println("Type *a* to move servomotor.");
            Serial.println("Type *b* to move servomotor with a required speed.");
            Serial.println("Type *c* to set endless mode of servomotor.");
            Serial.println("Type *d* to turn servomotor.");
            Serial.println("Type *e* to save instruction 'move servomotor'.");
            Serial.println("Type *f* to save instruction 'move servomotor with a required speed'.");
            Serial.println("Type *g* for servomotor to execute saved instruction.");
            stateServomotor=dutyMovements;
          break;
          
          default :
            stateServomotor=idle;
          break;
        }
      }
      else stateServomotor=duty;
    break;

    case dutyID : 
      if (serialAvailable) {
        strSerial.toCharArray(strBluetooth_i,BLUETOOTH_MAX_LENGTH);
        sprintf(strBluetooth,"S,%lu,%lu,I,%s",servomotorBaudrate,servomotorID,strBluetooth_i);
        bluetoothSerial.println(strBluetooth);
        
        Serial.print(strSerial);
        Serial.println(".");
        serialAvailable=0;

        servomotorID=strSerial.toInt();
        
        Serial.println("Servomotor of interest has been controlled.");
        Serial.println("Do you want to change servomotor of interest ? Type *n* for no or *y* for yes.");
        stateServomotor=done;
      }
      else stateServomotor=dutyID;
    break;

    case dutyBaudrate :
      if (serialAvailable) {
        strSerial.toCharArray(strBluetooth_i,BLUETOOTH_MAX_LENGTH);
        sprintf(strBluetooth,"S,%lu,%lu,B,%s",servomotorBaudrate,servomotorID,strBluetooth_i);
        bluetoothSerial.println(strBluetooth);
        
        Serial.print(strSerial);
        Serial.println(".");
        serialAvailable=0;

        servomotorBaudrate=strSerial.toInt();
        
         Serial.println("Servomotor of interest has been controlled.");
        Serial.println("Do you want to change servomotor of interest ? Type *n* for no or *y* for yes.");
        stateServomotor=done;
      }
      else stateServomotor=dutyBaudrate;
    break;

    case dutyMovements :
      if (serialAvailable) {
        strSerial.toCharArray(strBluetooth,BLUETOOTH_MAX_LENGTH);
        Serial.print("You typed *");
        Serial.print(strSerial);
        Serial.println("*.");
        serialAvailable=0;

        switch (strSerial[0]) {
          case 'A' :
          case 'a' :
            Serial.print("Type position between 0 and 1023 for servomotor to move to : ");
            stateServomotor=dutyMove;
          break;

          case 'B' :
          case 'b' :
            Serial.print("Type speed between 0 and 1023 for servomotor movement : ");
            stateServomotor=dutyMoveSpeed;
          break;

          case 'C' :
          case 'c' :
            Serial.print("Type *0* to turn OFF or *1* to turn ON endless mode of servomotor : ");
            stateServomotor=dutyEndless;
          break;

          case 'D' :
          case 'd' :
            Serial.print("Type negative value between 0 and 1023 for servomotor to turn clockwise or positive value to turn counterclockwise : ");
            stateServomotor=dutyTurn;
          break;

          case 'E' :
          case 'e' :
            Serial.print("Type position between 0 and 1023 for servomotor to move to : ");
            stateServomotor=dutyMoveRW;
          break;

          case 'F' :
          case 'f' :
            Serial.print("Type speed between 0 and 1023 for servomotor movement : ");
            stateServomotor=dutyMoveSpeedRW;
          break;

          case 'G' :
          case 'g' :
            sprintf(strBluetooth,"A,%lu,%lu",servomotorBaudrate,servomotorID);
            bluetoothSerial.println(strBluetooth);
            
            Serial.println("Servomotor of interest has been controlled.");
            Serial.println("Do you want to change servomotor of interest ? Type *n* for no or *y* for yes.");
            stateServomotor=done;
          break;

          default :
            stateServomotor=idle;
          break;
        }
      }
      else stateServomotor=dutyMovements;
    break;

    case dutyMove :
      if (serialAvailable) {
        strSerial.toCharArray(strBluetooth_i,BLUETOOTH_MAX_LENGTH);
        sprintf(strBluetooth,"M,%lu,%lu,M,M,%s",servomotorBaudrate,servomotorID,strBluetooth_i);
        bluetoothSerial.println(strBluetooth);
        
        Serial.print(strSerial);
        Serial.println(".");
        serialAvailable=0;
        
        Serial.println("Servomotor of interest has been controlled.");
        Serial.println("Do you want to change servomotor of interest ? Type *n* for no or *y* for yes.");
        stateServomotor=done;
      }
      else stateServomotor=dutyMove;
    break;

    case dutyMoveSpeed :
      if (serialAvailable) {
        strSerial.toCharArray(strBluetooth_i,BLUETOOTH_MAX_LENGTH);
        sprintf(strBluetooth,"M,%lu,%lu,S,S,%s",servomotorBaudrate,servomotorID,strBluetooth_i);
        bluetoothSerial.println(strBluetooth);
        
        Serial.print(strSerial);
        Serial.println(".");
        Serial.print("Type position between 0 and 1023 for servomotor to move to : ");
        serialAvailable=0;
        
        stateServomotor=dutyMove;
      }
      else stateServomotor=dutyMoveSpeed;
    break;

    case dutyEndless :
      if (serialAvailable) {
        strSerial.toCharArray(strBluetooth_i,BLUETOOTH_MAX_LENGTH);
        sprintf(strBluetooth,"E,%lu,%lu,%s",servomotorBaudrate,servomotorID,strBluetooth_i);
        bluetoothSerial.println(strBluetooth);
        
        Serial.print(strSerial);
        Serial.println(".");
        serialAvailable=0;
        
        Serial.println("Servomotor of interest has been controlled.");
        Serial.println("Do you want to change servomotor of interest ? Type *n* for no or *y* for yes.");
        stateServomotor=done;
      }
      else stateServomotor=dutyEndless;
    break;

    case dutyTurn :
      if (serialAvailable) {
        strSerial.toCharArray(strBluetooth_i,BLUETOOTH_MAX_LENGTH);
        sprintf(strBluetooth,"T,%lu,%lu,%s",servomotorBaudrate,servomotorID,strBluetooth_i);
        bluetoothSerial.println(strBluetooth);
        
        Serial.print(strSerial);
        Serial.println(".");
        serialAvailable=0;
        
        Serial.println("Servomotor of interest has been controlled.");
        Serial.println("Do you want to change servomotor of interest ? Type *n* for no or *y* for yes.");
        stateServomotor=done;
      }
      else stateServomotor=dutyTurn;
    break;

    case dutyMoveRW :
      if (serialAvailable) {
        strSerial.toCharArray(strBluetooth_i,BLUETOOTH_MAX_LENGTH);
        sprintf(strBluetooth,"M,%lu,%lu,M,W,%s",servomotorBaudrate,servomotorID,strBluetooth_i);
        bluetoothSerial.println(strBluetooth);
        
        Serial.print(strSerial);
        Serial.println(".");
        serialAvailable=0;
        
      Serial.println("Servomotor of interest has been controlled.");
      Serial.println("Do you want to change servomotor of interest ? Type *n* for no or *y* for yes.");
        stateServomotor=done;
      }
      else stateServomotor=dutyMoveRW;
    break;

    case dutyMoveSpeedRW :
      if (serialAvailable) {
        strSerial.toCharArray(strBluetooth_i,BLUETOOTH_MAX_LENGTH);
        sprintf(strBluetooth,"M,%lu,%lu,S,W,%s",servomotorBaudrate,servomotorID,strBluetooth_i);
        bluetoothSerial.println(strBluetooth);
        
        Serial.print(strSerial);
        Serial.println(".");
        Serial.print("Type position between 0 and 1023 for servomotor to move to : ");
        serialAvailable=0;
        
        stateServomotor=dutyMoveRW;
      }
      else stateServomotor=dutyMoveSpeedRW;
    break;

    case done :
      if (serialAvailable) {
        strSerial.toCharArray(strBluetooth,BLUETOOTH_MAX_LENGTH);
        Serial.print("You typed *");
        Serial.print(strSerial);
        Serial.println("*.");
        serialAvailable=0;

        if (strSerial[0]=='n') {
          Serial.println("Type *1* to reset servomotor.");
          Serial.println("Type *2* to set ID of servomotor.");
          Serial.println("Type *3* to set baudrate of servomotor.");
          Serial.println("Type *4* to try movements of servomotor.");
          
          stateServomotor=duty;
        }
        else stateServomotor=idle;
      }
      else stateServomotor=done;
    break;
  }
}
