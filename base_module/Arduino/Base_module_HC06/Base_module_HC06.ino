#include "DynamixelMotor.h"
#include <SoftwareSerial.h>

#define BLUETOOTH_TX_PIN 2
#define AX18_DIRECTION_PIN 3
#define BLUETOOTH_RX_PIN 4
#define AX18_RX_PIN 10
#define AX18_TX_PIN 11

#define AX18_DEFAULT_SPEED 512
#define AX18_LED_OFF LOW
#define AX18_LED_ON HIGH
#define AX18_MAX_POSITION 1023
#define AX18_MIN_POSITION 0
#define AX18_POSITIVE_TURN LEFT
#define AX18_NEGATIVE_TURN RIGHT

#define BLUETOOTH_UART_SPEED 9600

#define READ_DELAY 250
#define WRITE_DELAY 250

bool bluetoothAvailable;
String strBluetooth;

SoftwareDynamixelInterface servomotorSerial(AX18_RX_PIN,AX18_TX_PIN,AX18_DIRECTION_PIN);

SoftwareSerial bluetoothSerial(BLUETOOTH_RX_PIN,BLUETOOTH_TX_PIN);

void setup() {
  bluetoothSerial.begin(BLUETOOTH_UART_SPEED);
}

void loop() {    
  if (bluetoothSerial.available()) {
    strBluetooth=bluetoothSerial.readStringUntil('\n');
    bluetoothAvailable=1;
  }

  automateBluetooth();
}

void automateBluetooth() {
  typedef enum {idle,servomotorReset,servomotorAction,servomotorMove,servomotorMoveRW,servomotorSetBaudrate,servomotorSetEndless,servomotorSetID,servomotorTurn} typeState;
  static typeState stateBluetooth=idle;
  
  static bool speedAvailable;

  static unsigned char servomotorID;
  static long servomotorBaudrate,servomotorNewValue;
  static int servomotorNewSpeed;

  switch (stateBluetooth) {
    case idle :      
      if (bluetoothAvailable) {
        int commaIndex=strBluetooth.indexOf(',');
    
        String strLetter=strBluetooth.substring(0,commaIndex);
      
        int secondCommaIndex=strBluetooth.indexOf(',',commaIndex+1);
      
        String strServomotorBaudrate=strBluetooth.substring(commaIndex+1,secondCommaIndex);
        servomotorBaudrate=(long)strServomotorBaudrate.toInt();
      
        if (strLetter=="A") {
          String strServomotorID=strBluetooth.substring(secondCommaIndex+1);
          servomotorID=(unsigned char)strServomotorID.toInt();
          
          bluetoothSerial.end();
          
          stateBluetooth=servomotorAction;
        }
        else if (strLetter=="E") { 
          int thirdCommaIndex=strBluetooth.indexOf(',',secondCommaIndex+1);
          String strServomotorID=strBluetooth.substring(secondCommaIndex+1,thirdCommaIndex);
          servomotorID=(unsigned char)strServomotorID.toInt();
          String strServomotorNewValue=strBluetooth.substring(thirdCommaIndex+1);
          servomotorNewValue=(long)strServomotorNewValue.toInt();
          
          bluetoothSerial.end();
          
          stateBluetooth=servomotorSetEndless;
        }
        else if (strLetter=="M") { 
          int thirdCommaIndex=strBluetooth.indexOf(',',secondCommaIndex+1);
          String strServomotorID=strBluetooth.substring(secondCommaIndex+1,thirdCommaIndex);
          servomotorID=(unsigned char)strServomotorID.toInt();
          int fourthCommaIndex=strBluetooth.indexOf(',',thirdCommaIndex+1);
          String strSecondLetter=strBluetooth.substring(thirdCommaIndex+1,fourthCommaIndex);
          int fifthCommaIndex=strBluetooth.indexOf(',',fourthCommaIndex+1);
          String strThirdLetter=strBluetooth.substring(fourthCommaIndex+1,fifthCommaIndex);
          String strServomotorNewValue=strBluetooth.substring(fifthCommaIndex+1);
          
          if (strSecondLetter=="M") {
            servomotorNewValue=(long)strServomotorNewValue.toInt();

            bluetoothSerial.end();
            
            if (strThirdLetter=="M") stateBluetooth=servomotorMove;
            else if (strThirdLetter=="W") stateBluetooth=servomotorMoveRW;
          }
          else if (strSecondLetter=="S") {
            servomotorNewSpeed=strServomotorNewValue.toInt();
            speedAvailable=1;
          }
        }
        else if (strLetter=="R") {
          String strServomotorID=strBluetooth.substring(secondCommaIndex+1);
          servomotorID=(unsigned char)strServomotorID.toInt();
          
          bluetoothSerial.end();
          
          stateBluetooth=servomotorReset;
        }
        else if (strLetter=="S") { 
          int thirdCommaIndex=strBluetooth.indexOf(',',secondCommaIndex+1);
          String strServomotorID=strBluetooth.substring(secondCommaIndex+1,thirdCommaIndex);
          servomotorID=(unsigned char)strServomotorID.toInt();
          int fourthCommaIndex=strBluetooth.indexOf(',',thirdCommaIndex+1);
          String strSecondLetter=strBluetooth.substring(thirdCommaIndex+1,fourthCommaIndex);
          String strServomotorNewValue=strBluetooth.substring(fourthCommaIndex+1);
          servomotorNewValue=(long)strServomotorNewValue.toInt();
          
          bluetoothSerial.end();
          
          if (strSecondLetter=="B") stateBluetooth=servomotorSetBaudrate;
          else if (strSecondLetter=="I") stateBluetooth=servomotorSetID;
        }
        else if (strLetter=="T") {
          int thirdCommaIndex=strBluetooth.indexOf(',',secondCommaIndex+1);
          String strServomotorID=strBluetooth.substring(secondCommaIndex+1,thirdCommaIndex);
          servomotorID=(unsigned char)strServomotorID.toInt();
          String strServomotorNewValue=strBluetooth.substring(thirdCommaIndex+1);
          servomotorNewValue=(long)strServomotorNewValue.toInt();
          
          bluetoothSerial.end();
          
          stateBluetooth=servomotorTurn;
        }

        bluetoothAvailable=0;
      }
      else stateBluetooth=idle;
    break; 
    
    case servomotorAction :
      if (servomotorID>0&&servomotorBaudrate>0) {
        DynamixelMotor servomotor(servomotorSerial,servomotorID);
        servomotorSerial.begin(servomotorBaudrate);
        servomotor.init();
        servomotor.enableTorque();
        servomotor.action();
        delay(WRITE_DELAY);
        servomotorSerial.end();
      }
      
      bluetoothSerial.begin(BLUETOOTH_UART_SPEED);
      stateBluetooth=idle;
    break;

    case servomotorMove :
      if (servomotorID>0&&servomotorBaudrate>0) {
        DynamixelMotor servomotor(servomotorSerial,servomotorID);
        servomotorSerial.begin(servomotorBaudrate);
        servomotor.init();
        servomotor.enableTorque();
        servomotor.jointMode(AX18_MIN_POSITION,AX18_MAX_POSITION);
        delay(WRITE_DELAY);
        if (speedAvailable) {
          servomotor.speed(servomotorNewSpeed);
          speedAvailable=0;
        }
        else servomotor.speed(AX18_DEFAULT_SPEED);
        delay(WRITE_DELAY);
        servomotor.goalPosition(servomotorNewValue);
        delay(WRITE_DELAY);
        servomotorSerial.end();
      }

      bluetoothSerial.begin(BLUETOOTH_UART_SPEED);
      stateBluetooth=idle;
    break;

    case servomotorMoveRW :
      if(servomotorID>0&&servomotorBaudrate>0)
      {
        DynamixelMotor servomotor(servomotorSerial,servomotorID);
        servomotorSerial.begin(servomotorBaudrate);
        servomotor.init();
        if (speedAvailable) {
          servomotor.regWrite(DYN_ADDRESS_GOAL_SPEED,servomotorNewSpeed);
          speedAvailable=0;
        }
        else servomotor.regWrite(DYN_ADDRESS_GOAL_SPEED,AX18_DEFAULT_SPEED);
        delay(WRITE_DELAY);
        servomotor.regWrite(DYN_ADDRESS_GOAL_POSITION,servomotorNewValue);
        delay(WRITE_DELAY);
        servomotorSerial.end();
      }

      bluetoothSerial.begin(BLUETOOTH_UART_SPEED);
      stateBluetooth=idle;
    break;
    
    case servomotorReset : 
      if (servomotorID>0&&servomotorBaudrate>0) {
        DynamixelMotor servomotor(servomotorSerial,servomotorID);
        servomotorSerial.begin(servomotorBaudrate);
        servomotor.init();
        servomotor.reset();
        delay(WRITE_DELAY);
        servomotorSerial.end();
      }

      bluetoothSerial.begin(BLUETOOTH_UART_SPEED);
      stateBluetooth=idle;
    break;

    case servomotorSetBaudrate :
      if (servomotorID>0&&servomotorBaudrate>0) {
        DynamixelMotor servomotor(servomotorSerial,servomotorID);
        servomotorSerial.begin(servomotorBaudrate);
        servomotor.init();
        servomotor.communicationSpeed(servomotorNewValue);
        delay(WRITE_DELAY);
        servomotorSerial.end();
      }

      bluetoothSerial.begin(BLUETOOTH_UART_SPEED);
      stateBluetooth=idle;
    break;

    case servomotorSetEndless :      
      if (servomotorID>0&&servomotorBaudrate>0) {
        DynamixelMotor servomotor(servomotorSerial,servomotorID);
        servomotorSerial.begin(servomotorBaudrate);
        servomotor.init();
        servomotor.enableTorque();
        if((bool)servomotorNewValue) servomotor.wheelMode();
        else {
          servomotor.jointMode(AX18_MIN_POSITION,AX18_MAX_POSITION);
          delay(WRITE_DELAY);
          servomotor.speed(0);
        }
        delay(WRITE_DELAY);
        servomotorSerial.end();
      }

      bluetoothSerial.begin(BLUETOOTH_UART_SPEED);
      stateBluetooth=idle;
    break;

    case servomotorSetID :
      if (servomotorID>0&&servomotorBaudrate>0) {
        DynamixelMotor servomotor(servomotorSerial,servomotorID);
        servomotorSerial.begin(servomotorBaudrate);
        servomotor.init();
        servomotor.changeID((unsigned char)servomotorNewValue);
        delay(WRITE_DELAY);
        servomotorSerial.end();
      }

      bluetoothSerial.begin(BLUETOOTH_UART_SPEED);
      stateBluetooth=idle;
    break;

    case servomotorTurn :
      if (servomotorID>0&&servomotorBaudrate>0) {
        DynamixelMotor servomotor(servomotorSerial,servomotorID);
        servomotorSerial.begin(servomotorBaudrate);
        servomotor.init();
        servomotor.speed(servomotorNewValue);
        delay(WRITE_DELAY);
        servomotorSerial.end();
      }

      bluetoothSerial.begin(BLUETOOTH_UART_SPEED);
      stateBluetooth=idle;
    break;    
  }
}

