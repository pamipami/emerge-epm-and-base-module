#ifndef DYNAMIXEL_INTERFACE_IMPL_H
#define DYNAMIXEL_INTERFACE_IMPL_H

#include "DynamixelInterface.h"
#include <Arduino.h>
#include <SoftwareSerial.h>

template<class T>
class DynamixelInterfaceImpl:public DynamixelInterface
{
	private:
	// Switch stream to read mode
	void readMode();
	
	// Switch stream to write mode
	void writeMode();
	
	public:
	
	/* aStreamController = stream controller that abstracts real stream
	 * aTxPin = pin number of the TX pin
	 * aDirectionPin = direction pin (default value = NO_DIR_PORT) */
	DynamixelInterfaceImpl(T &aStream,uint8_t aTxPin,uint8_t aDirectionPin);
	
	// Delete stream if it is owned by the instance
	~DynamixelInterfaceImpl();
	
	// Start the interface with desired baudrate, call once before using the interface
	void begin(unsigned long aBaud);
	
	// Send a packet on bus and wait for the packet to be completly sent
	void sendPacket(const DynamixelPacket &aPacket);

	// Receive a packet on bus. Timeout depends of timeout of the underlying stream. Return error code in case of communication error (timeout, checksum error, ...).
	void receivePacket(DynamixelPacket &aPacket,uint8_t answerSize = 0);

	// Stop the interface
  void end();
	
	static const uint8_t NO_DIR_PORT=255;
	
	private:
	
	T &mStream;
	const uint8_t mDirectionPin;

	protected:
	const uint8_t mTxPin;
};

class HardwareDynamixelInterface:public DynamixelInterfaceImpl<HardwareSerial>
{
	public:
	HardwareDynamixelInterface(HardwareSerial &aSerial,uint8_t aDirectionPin=NO_DIR_PORT);
	~HardwareDynamixelInterface();
};

class SoftwareDynamixelInterface:public DynamixelInterfaceImpl<SoftwareSerial>
{
	public:
	SoftwareDynamixelInterface(uint8_t aRxPin,uint8_t aTxPin,uint8_t aDirectionPin=NO_DIR_PORT);
	~SoftwareDynamixelInterface();
  
	private:
	SoftwareSerial mSoftSerial;
};

#endif
