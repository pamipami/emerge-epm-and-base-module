#ifndef DYNAMIXEL_H
#define DYNAMIXEL_H

#include <stdint.h>
#include <stdlib.h> 

// Type of Dynamixel device ID
typedef uint8_t DynamixelID;
// Type of Dynamixel status code
typedef uint8_t DynamixelStatus;
// Type of Dynamixel instructions
typedef uint8_t DynamixelInstruction;

// ID for broadcast
#define BROADCAST_ID 0xFE


// Dynamixel intruction values
enum DynInstruction
{
	DYN_PING		    = 0x01,
	DYN_READ		    = 0x02,
	DYN_WRITE		    = 0x03,
	DYN_REG_WRITE	  = 0x04,
	DYN_ACTION		  = 0x05,
	DYN_RESET		    = 0x06,
	DYN_SYNC_WRITE  = 0x83
};

// Dynamixel status values
/* If (status&DYN_STATUS_COM_ERROR)==0 , the value is the the status returned by the motor, describing its internal error. 
 * If (status&DYN_STATUS_COM_ERROR)==1, there was an error during communication, and the value describe that error.
 * DYN_STATUS_CHECKSUM_ERROR may appear in both cases, in the first case, it means there was an error in the checksum of the instruction packet, in second case in the response packet. */
enum DynStatus
{
	DYN_STATUS_OK				            = 0,
	
	DYN_STATUS_INPUT_VOLTAGE_ERROR  = 1,
	DYN_STATUS_ANGLE_LIMIT_ERROR		= 2,
	DYN_STATUS_OVERHEATING_ERROR		= 4,
	DYN_STATUS_RANGE_ERROR			    = 8,
	DYN_STATUS_CHECKSUM_ERROR		    = 16,
	DYN_STATUS_OVERLOAD_ERROR		    = 32,
	DYN_STATUS_INSTRUCTION_ERROR		= 64,
	
	DYN_STATUS_TIMEOUT			        = 1,
	
	DYN_STATUS_COM_ERROR			      = 128,
	DYN_STATUS_INTERNAL_ERROR		    = 255
};


struct DynamixelPacket
{
	DynamixelPacket(){}
	DynamixelPacket(uint8_t aID, uint8_t aInstruction, uint8_t aLength, const uint8_t *aData, uint8_t aAddress=255, uint8_t aDataLength=255, uint8_t aIDListSize=0, const uint8_t *aIDList=NULL):
		mID(aID), mIDListSize(aIDListSize), mIDList(const_cast<uint8_t*>(aIDList)), mLength(aLength), mInstruction(aInstruction), mAddress(aAddress), mDataLength(aDataLength), mData(const_cast<uint8_t*>(aData))
	{
		mCheckSum=checkSum();
	}
	
	// Packet ID
	DynamixelID mID;
	// ID list
	uint8_t mIDListSize;
	DynamixelID *mIDList;
	/// Packet length
	uint8_t mLength;
	// Packet instruction or status (default value = 0)
	union{
		DynamixelInstruction mInstruction;
		DynamixelStatus mStatus;
	};
	// Address to read/write (default value = 255)
	uint8_t mAddress;
	// Length of data to read/write (default value = 255)
	uint8_t mDataLength;
	// Pointer to packet parameter (default value = NULL)
	uint8_t *mData;
	// Packet checksum
	uint8_t mCheckSum;
	
	// Checksum value
	uint8_t checkSum();
};



// Dynamixel control table addresses
enum DynCommonAddress
{
	// Model number        / 16 bits / R
	DYN_ADDRESS_MODEL     = 0x00,
	// Version of Firmware / 8 bits  / R
	DYN_ADDRESS_FIRMWARE  = 0x02,
	// ID                  / 8 bits  / RW
	DYN_ADDRESS_ID        = 0x03,
	// Baud Rate           / 8 bits  / RW
	DYN_ADDRESS_BAUDRATE  = 0x04,
	// Return Delay Time   / 8 bits  / RW
	DYN_ADDRESS_RDT       = 0x05,
	// Status Return Level / 8 bits  / RW
	/* 0 = ping only / 1 = ping + read / 2 = all instructions */ 
	DYN_ADDRESS_SRL       = 0x10 
};

// Dynamixel motor control table addresses
enum DynMotorAddress
{
	// CW Angle Limit            / 16 bits / RW
	DYN_ADDRESS_CW_LIMIT		        = 0x06,
	// CCW Angle Limit           / 16 bits / RW
	DYN_ADDRESS_CCW_LIMIT		        = 0x08,
  // Highest Limit Temperature / 8 bits  / RW
  DYN_ADDRESS_MAX_TEMPERATURE     = 0x0B,
  // Lowest Limit Voltage      / 8 bits  / RW
  DYN_ADDRESS_MIN_VOLTAGE         = 0x0C,
  // Highest Limit Voltage     / 8 bits  / RW
  DYN_ADDRESS_MAX_VOLTAGE         = 0x0D,
	// Max Torque                / 16 bits / RW
	DYN_ADDRESS_MAX_TORQUE		      = 0x0E,
  // Alarm LED                 / 8 bits  / RW
  DYN_ADDRESS_ALARM_LED           = 0x11,
  // Alarm Shutdown            / 8 bits  / RW
  DYN_ADDRESS_ALARM_SHUTDOWN      = 0x12,
	// Torque Enable             / 8 bits  / RW
	DYN_ADDRESS_ENABLE_TORQUE	      = 0x18,
	// LED                       / 8 bits  / RW
	DYN_ADDRESS_LED			            = 0x19,
	// CW Compliance Margin      / 8 bits  / RW
	DYN_ADDRESS_CW_COMP_MARGIN	    = 0x1A,
	// CCW Compliance Margin     / 8 bits  / RW
	DYN_ADDRESS_CCW_COMP_MARGIN     = 0x1B,
	// CW Compliance Slope       / 8 bits  / RW
	DYN_ADDRESS_CW_COMP_SLOPE	      = 0x1C,
	// CCW Compliance Slope      / 8 bits  / RW
	DYN_ADDRESS_CCW_COMP_SLOPE      = 0x1D,
	// Goal Position             / 16 bits / RW
	DYN_ADDRESS_GOAL_POSITION	      = 0x1E,
	// Moving Speed              / 16 bits / RW
	DYN_ADDRESS_GOAL_SPEED		      = 0x20,
  // Torque Limit              / 16 bits / RW
	DYN_ADDRESS_TORQUE_LIMIT	      = 0x22,
	// Present Position          / 16 bits / R
	DYN_ADDRESS_CURRENT_POSITION    = 0x24,
	// Present Speed             / 16 bits / R
  DYN_ADDRESS_CURRENT_SPEED       = 0x26,
  // Present Load              / 16 bits / R
  DYN_ADDRESS_CURRENT_LOAD        = 0x28,
  // Present Voltage           / 8 bits  / R
  DYN_ADDRESS_CURRENT_VOLTAGE     = 0x2A,
  // Present Temperature       / 8 bits  / R
  DYN_ADDRESS_CURRENT_TEMPERATURE = 0x2B,
  // Registered                / 8 bits  / R
  DYN_ADDRESS_REGISTERED          = 0x2C,
  // Moving                    / 8 bits  / R
	DYN_ADDRESS_MOVING		          = 0x2E,
  // Lock                      / 8 bits  / RW
  DYN_ADDRESS_LOCK                = 0x2F,
  // Punch                     / 16 bits / RW
  DYN_ADDRESS_PUNCH               = 0x30
};

// Dynamixel model number values
enum DynModel
{
	DYN_MODEL_AX12A	  = 0x0C,
	DYN_MODEL_AX12W	  = 0x2C,
	DYN_MODEL_AX18A	  = 0x12,
	
	DYN_MODEL_DX113	  = 0x71,
	DYN_MODEL_DX114	  = 0x74,
	DYN_MODEL_DX117	  = 0x75,
	
	DYN_MODEL_RX10	  = 0x0A,
	DYN_MODEL_RX24F	  = 0x18,
	DYN_MODEL_RX28	  = 0x1C,
	DYN_MODEL_RX64	  = 0x40,
	
	DYN_MODEL_EX106	  = 0x6B,
	
	DYN_MODEL_MX12W	  = 0x68,
	DYN_MODEL_MX28T	  = 0x1D,
	DYN_MODEL_MX28R	  = 0x1D,
	DYN_MODEL_MX64T	  = 0x36,
	DYN_MODEL_MX64R	  = 0x36,
	DYN_MODEL_MX106T  = 0x40,
	DYN_MODEL_MX106R  = 0x40,
	
	DYN_MODEL_AXS1	  = 0x0D
};

#endif
